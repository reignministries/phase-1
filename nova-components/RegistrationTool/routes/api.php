<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Registration; 
use App\Form;


/*
|--------------------------------------------------------------------------
| Tool API Routes
|--------------------------------------------------------------------------
|
| Here is where you may register API routes for your tool. These routes
| are loaded by the ServiceProvider of your tool. You're free to add
| as many additional routes to this file as your tool may require.
|
 */

Route::get('/test', function (Request $request) {
    return request();
});

Route::post('{id}/update-forms', function (Request $request, $id) {
    $reg = Registration::find($id);
    $oldForms = $reg->forms;
    foreach ($request->all() as $new) {
        $form = $oldForms->where('id', $new['id'])->first();
        $form->status->update([
            'completed' => $new['status']['completed'],
            'error' => $new['status']['error'],
            'error_message' => $new['status']['error_message']
        ]);
        $form->status->save();
    }
});

Route::get('/forms/all', function (){
    return Form::all();
});


