<?php

namespace Reign\RegistrationTool;

use Laravel\Nova\ResourceTool;

class RegistrationTool extends ResourceTool
{
    /**
     * Get the displayable name of the resource tool.
     *
     * @return string
     */
    public function name()
    {
         return 'Forms';
    }

    /**
     * Get the component name for the resource tool.
     *
     * @return string
     */
    public function component()
    {
        return 'RegistrationTool';
    }
}
