
import PrettyCheck from 'pretty-checkbox-vue/check'
 
Nova.booting((Vue, router) => {

    Vue.component('p-check', PrettyCheck)
    Vue.component('attach-forms', require('./components/attachForms.vue'))
    Vue.component('RegistrationTool', require('./components/Tool'));
})
