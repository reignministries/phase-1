<?php

namespace Reign\RoyalServants\Http\Middleware;

use Reign\RoyalServants\RoyalServants;

class Authorize
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return \Illuminate\Http\Response
     */
    public function handle($request, $next)
    {
        return resolve(RoyalServants::class)->authorize($request) ? $next($request) : abort(403);
    }
}
