<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registrations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('trip_year');
            $table->unique(['user_id','trip_year']);
            // Misc. Info Related to that specific year
            $table->integer('division_id');
            $table->integer('trip_id');
            $table->integer('role_id');
            // Timeline Progress
            $table->json('timeline_status')->nullable();
            // Individual Cost Items
            $table->integer('domestic_airfair_reimbursement')->nullable();
            $table->integer('rs_fund')->nullable();
            $table->integer('begining_balance')->nullable();
            $table->integer('pack_deposit')->nullable();
            $table->integer('visa')->nullable();

            
            // Created at and Updated At 
            $table->timestamps();
        });               

        Schema::create('form_registration', function (Blueprint $table) {
            $table->unsignedInteger('registration_id');
            $table->unsignedInteger('form_id');
            $table->primary(['form_id','registration_id']);
            $table->boolean('completed')->default(false);
            $table->boolean('error')->default(false);
            $table->text('error_message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registrations');
    }
}
