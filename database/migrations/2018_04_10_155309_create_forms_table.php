<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('due_date');
            $table->json('meta')->nullable();
            $table->timestamps();
        });

         Schema::create('division_form', function (Blueprint $table) {
            $table->unsignedInteger('division_id');
            $table->unsignedInteger('form_id');
            $table->primary(['division_id','form_id']);
        }); 
         
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forms');
    }
}
