<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'first' => $faker->firstName,
        'last'=> $faker->lastName,
        'role_id'=> '2',
        'email' => $faker->unique()->safeEmail,
        'p_code' => 'P'.$faker->randomNumber($nbdigits = 4, $strict = true),
        'password' => bcrypt('123456'), // secret
        'remember_token' => str_random(10),
    ];
})->afterCreating(App\User::class, function ($user, $faker) {
    $user->registrations()->save(factory(App\Registration::class)->make());
    $user->donations()->saveMany(factory(App\Donation::class, 30)->make());
});