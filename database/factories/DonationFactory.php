<?php

use Faker\Generator as Faker;

$factory->define(App\Donation::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'amount' => $faker->numberBetween($min = 10, $max = 100),
        'donation_date' => $faker->dateTime($max = 'now', $timezone = null),
        'reference_number' => $faker->ean13,
        'p_code' => 'P0021',
    ];
});
