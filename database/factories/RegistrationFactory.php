<?php

use Faker\Generator as Faker;

$factory->define(App\Registration::class, function (Faker $faker) {
    return [
        'user_id' => App\User::all()->random()->id,
        'trip_year'=> '2018',
        'division_id'=> '1',
        'role_id'=> '2',
        'trip_id'=> App\Trip::all()->random()->id,      

    ];
});
