<?php

use Faker\Generator as Faker;

$factory->define(App\Trip::class, function (Faker $faker) {
    return [         
            'name'=> $faker->country,
            'base_cost' => $faker->numberBetween($min = 2000, $max = 4000),
            'student_spending' => $faker->numberBetween($min = 100, $max = 300),
            'staff_spending' => $faker->numberBetween($min = 300, $max = 500),
            'year' => '2018',
            'visa'=> $faker->numberBetween($min = 0, $max = 50),
            'fuel_surcharge' => $faker->numberBetween($min = 0, $max = 90),
  
    ];
});
