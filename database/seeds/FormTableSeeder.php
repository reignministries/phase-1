<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class FormTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          DB::table('forms')->insert(
        array(
            'name'=>'Health History',
            'due_date' => '5/23/2018',               
        ));
       
        DB::table('forms')->insert(
        array(
            'name'=>'Vaccination',
            'due_date' => '5/23/2018',               
        ));
        DB::table('forms')->insert(
        array(
            'name'=>'Physician Form',
            'due_date' => '5/23/2018',
        ));
        DB::table('forms')->insert(
        array(
            'name'=>'Domestic Travel',
            'due_date' => '5/23/2018',               
        ));
        DB::table('forms')->insert(
        array(
            'name'=>'Background Check',
            'due_date' => '5/23/2018', 
        ));
        DB::table('forms')->insert(
        array(
            'name'=>'Terms and Condiditions',
            'due_date' => '5/23/2018',               
        ));
        DB::table('forms')->insert(
        array(
            'name'=>'Insurance',
            'due_date' => '5/23/2018',               
        ));
        DB::table('forms')->insert(
        array(
            'name'=>'Passport Copy',
            'due_date' => '5/23/2018',               
        ));

        // Attaches the Forms to a division
        DB::table('division_form')->insert(
            array(
                'division_id'=>1,
                'form_id'=>1
            ));
        DB::table('division_form')->insert(
            array(
                'division_id'=>1,
                'form_id'=>2
            ));
        DB::table('division_form')->insert(
            array(
                'division_id'=>1,
                'form_id'=>3
            ));
        DB::table('division_form')->insert(
            array(
                'division_id'=>1,
                'form_id'=>4
            ));
        DB::table('division_form')->insert(
            array(
                'division_id'=>1,
                'form_id'=>5
            ));
        DB::table('division_form')->insert(
            array(
                'division_id'=>1,
                'form_id'=>6
            ));
        DB::table('division_form')->insert(
            array(
                'division_id'=>1,
                'form_id'=>7
            ));
        DB::table('division_form')->insert(
            array(
                'division_id'=>1,
                'form_id'=>8
            ));
      
    }




}
