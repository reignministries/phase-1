<?php

use Illuminate\Database\Seeder;
use App\Registration;
use App\Division;

class RegistrationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('registrations')->insert(
            array(
                'id'=> '1',
                'user_id'=> '23',
                'trip_id'=> '1',
                'role_id'=> '2',
                'division_id'=>'1',
                'trip_year'=> '2018',
                'domestic_airfair_reimbursement' => 200,
                'rs_fund'=> 30,
                'begining_balance' => 0,
                'pack_deposit' => 40,
                                
            )); 

            $reg = Registration::first();

            $forms = Division::find($reg->division_id)->forms()->select('id')->get();
              
      
            // TODO:: Add form attaching when a registration is created. 
              foreach($forms as $form){         
                  Registration::find($reg->id)->forms()->syncWithoutDetaching($form);
              }
            
            
    
    }
}
