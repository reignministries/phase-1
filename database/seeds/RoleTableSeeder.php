<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          // Roles Table Seed
        DB::table('roles')->insert(
        array(
            'id' => '1',
            'name' => 'admin',            
        ));
        DB::table('roles')->insert(
        array(
            'id' => '2',
            'name' => 'Student',  
            'settings' => '[{
                "prep_packet": "http://reignministries.org/Student-Prep-Packet",
                "special_form_access": true
            }]',
        ));
        DB::table('roles')->insert(
        array(
            'id' => '3',
            'name' => 'Staff',
            'settings' => '[{
                "prep_packet": "https://reignministries.org/Staff-Discipler-Prep-Packet"
            }]'           
        ));
        DB::table('roles')->insert(
        array(
            'id' => '4',
            'name' => 'Nehemiah',
            'settings' => '[{
                "prep_packet": "http://reignministries.org/nehemiah-prep-packet"
            }]'           
        ));
        DB::table('roles')->insert(
        array(
            'id' => '5',
            'name' => 'Parent',
            'settings' => '[{
                "prep_packet": "http://reignministries.org/Parent-Packet"
            }]'           
        ));
       
    }
}
