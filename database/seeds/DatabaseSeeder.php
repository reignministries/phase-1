<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call([
            RoleTableSeeder::class,
            FormTableSeeder::class,
            DivisionTableSeeder::class,
            RegistrationTableSeeder::class,
        ]);

        factory(App\Donation::class, 30)->create();
        factory(App\Trip::class, 8)->create();
        factory(App\User::class, 20)->create();




        DB::table('users')->insert(
            array(
                'id' => '23',
                'first' => 'Student',
                'middle' => 'Test',
                'last' => 'man',
                "address_1" => "4334 Queen Ave",
                "address_2" => null,
                "city" => "Minneapolis",
                "state" => "Minnesota",
                "zip_code" => "55369",
                'email' => 'cor@email.com',
                'password' => bcrypt('123456'),
                'p_code' => 'P0021',
                'role_id' => '2'
            )
        );
        DB::table('users')->insert(
            array(
                'id' => '43',
                'first' => 'Corin Admin',
                'email' => 'admin@email.com',
                'password' => bcrypt('123456'),
                'role_id' => '1'
            )
        );




    }
}
