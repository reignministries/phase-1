import Vuex from 'vuex'
import Vue from 'vue';
Vue.use(Vuex)

export const store = new Vuex.Store({
    strict: true,
    state: {
        reg,
        user,
        trip: reg.trip,
        donations,
        forms,
        stepStatus: reg.timeline_status
    },
    getters: {
        tripCost: (state) => {
            /**
             * Pulls together all of the various aspects that will add to the
             * total cost of the trip that needs to be raised regardless if 
             * it belongs to the trip or the users registration.
             */
            let trip = state.trip;
            let reg = state.reg;
            let role = state.user.role.name;
            // These two variables will be set by futher down the getter
            let spending = null;
            let visaCost = null;
            /**
             * Calculates what the spending amount should be based
             * on the participants role
             */
            if (role == "Student") {
                spending = trip.student_spending;
            } else if (role == "Staff") {
                spending = trip.staff_spending;
            } else {
                return console.error('missing role in trip cost spending');
            };
            /**
             * Checks to see if the visa should come from the trip 
             * table or the users registration table. 
             */
            if (reg.visa <= 0) {
                visaCost = trip.visa;
            } else {
                visaCost = reg.visa;
            }

            // TODO:: Move to Database
            var array = [{
                    "name": "Domestic Air Fair Reimbursement",
                    "cost": reg.domestic_airfair_reimbursement,
                },
                {
                    "name": "RS Fund",
                    "cost": reg.rs_fund,
                },
                {
                    "name": "Begining Balance",
                    "cost": -reg.begining_balance,
                },
                {
                    "name": "Pack Deposit",
                    "cost": reg.pack_deposit,
                },
                {
                    "name": "Spending Money",
                    "cost": spending,
                },
                {
                    "name": "Trip Cost",
                    "cost": trip.base_cost,
                },
                {
                    "name": "Visa Cost",
                    "cost": visaCost,
                },
            ];

            let costItems = _.orderBy(array, ['cost'], ['desc']);
            let costTotal = _.sumBy(array, 'cost')


            return {
                items: costItems,
                total: costTotal
            };
        },
        donations: (state, getters) => {
            let donationsRecieved = donations
            let amountRaised = _.sumBy(donationsRecieved, 'amount')
            var finishedSupportRaising = false


            if (getters.tripCost.total >= amountRaised) {
                finishedSupportRaising = false
            } else {
                finishedSupportRaising = true
            }

            return {
                recieved: donationsRecieved,
                total: amountRaised,
                finished: finishedSupportRaising
            }
        }



    },
    mutations: {
        updateLocalAvatar(state, url){
            state.user.avatar = url
        },
        updateLocalFirstName(state, name){
            state.user.first = name
        }
    }

})