import "./bootstrap";
import router from "./routes";


/**NPM Dependencies */
import Slideout from 'slideout';


//Vee Validate Error catching
Vue.prototype.$setErrorsFromResponse = function (errorResponse) {
    // only allow this function to be run if the validator exists
    if (!this.hasOwnProperty('$validator')) {
        return;
    }

    // clear errors
    this.$validator.errors.clear();

    // check if errors exist
    if (!errorResponse.hasOwnProperty('errors')) {
        return;
    }

    let errorFields = Object.keys(errorResponse.errors);

    // insert laravel errors
    errorFields.map(field => {
        let errorString = errorResponse.errors[field].join(', ');
        this.$validator.errors.add(field, errorString);
    });
};


/**
 * Register all Vue components
 */

// Template 
Vue.component('example-component', require('./components/ExampleComponent.vue'), );
Vue.component('card-component', require('./components/CardComponent.vue'));

// Dashboard/general 
Vue.component('timeline-component', require('./components/dashboard/TimelineComponent.vue'));

Vue.component('support-component', require('./components/dashboard/FullSupportComponent.vue'));
Vue.component('cost-component', require('./components/dashboard/CostComponent.vue'));

// Form 
Vue.component('detailed-reg-form', require('./components/forms/detailed-registration/DetailedRegFormComponent.vue'));

/**
 * View Components
 */
Vue.component('submit-button', require('./components/SubmitButton.vue'))
Vue.component('reign-header', require('./components/Header.vue'))
Vue.component('dashboard', require('./components/views/Dashboard.vue'))



/**
 * Vuex State Management
 */
import {
    store
} from './store/store'


/**
 * Load and attach a Vue instance
 */
const app = new Vue({
    store,
    el: '#app',
    name: 'App',
    router,
    data: {
        ready: false,
        // TODO::Move trip year to Database
        tripYear: 2018,
    },
    methods: {
        loaded() {
            this.ready = true;
        },

    },

    computed: {
        state() {
            return this.$store.state
        }
    },

    mounted() {
        this.loaded();
    },

});





/**
 * Slideout sidebar menu settings
 */
var slideout = new Slideout({
    'panel': document.getElementById('panel'),
    'menu': document.getElementById('menu'),
    'padding': 210,
    'tolerance': 70
});

// Toggle button
document.querySelector('.toggle-button').addEventListener('click', function () {
    slideout.toggle();
});

var fixed = document.querySelector('.fixed-header');

slideout.on('translate', function (translated) {
    fixed.style.transform = 'translateX(' + translated + 'px)';
});

slideout.on('beforeopen', function () {
    fixed.style.transition = 'transform 300ms ease';
    fixed.style.transform = 'translateX(210px)';
});

slideout.on('beforeclose', function () {
    fixed.style.transition = 'transform 300ms ease';
    fixed.style.transform = 'translateX(0px)';
});

slideout.on('open', function () {
    fixed.style.transition = '';
});

slideout.on('close', function () {
    fixed.style.transition = '';
});