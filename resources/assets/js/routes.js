import VueRouter from 'vue-router';


import Dashboard from './components/views/Dashboard.vue'
import Support from './components/views/Support.vue'
import MyAccount from './components/views/MyAccount.vue'

const routes = [{
        path: '/',
        redirect: '/dashboard'
    },
    {
        path: '/dashboard',
        component: Dashboard
    },
    {
        path: '/dashboard/support',
        component: Support
    },
    {
        path: '/my-account',
        component: MyAccount,
    }

]


export default new VueRouter({
    mode: 'history',
    routes,
})