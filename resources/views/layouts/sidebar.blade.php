{{-- Sidebar --}}
<div class="sidebar" id="menu">
    <div class="sidebar-container">
        <router-link to="/dashboard" class="trip-title">
            <h1>{!! $currentReg->trip->name !!}</h1>
            <h2>{!! $currentReg->trip->year !!}</h2>
        </router-link>

        <div class="sidebar-menu">
            <div class="sidebar-section">
                <div class="title">info</div>
                <ul class="list-unstyled">
                    <li>
                        <a href="https://reignministries.org/Mission-Trip-Schedule" target="_blank">
                            <span class="icon-padding">
                                <i class="fas fa-map fa-lg fa-fw"></i>
                            </span>Schedule
                        <i class="fas fa-external-link-alt fa-sm fa-fw"></i></a>

                    </li>
                    <li>
                        <a href="https://reignministries.org/mymissiontrip/pre-summer-letters/#1" target="_blank">
                            <span class="icon-padding">
                                <i class="fas fa-envelope fa-lg fa-fw"></i>
                            </span>Pre-Summer Letters
                        <i class="fas fa-external-link-alt fa-sm fa-fw"></i></a>
                    </li>
                    <li>
                        <a href="http://reignministries.org/Packing-List" target="_blank">
                            <span class="icon-padding">
                                <i class="fas fa-suitcase fa-lg fa-fw"></i>
                            </span>Packing List
                        <i class="fas fa-external-link-alt fa-sm fa-fw"></i></a>
                    </li>
                    <li>
                        <a href="http://reignministries.org/Reference-Sheet-Important-Dates" target="_blank">
                            <span class="icon-padding">
                                <i class="fas fa-file-alt fa-lg fa-fw"></i>
                            </span>Reference Sheet
                        <i class="fas fa-external-link-alt fa-sm fa-fw"></i></a>
                    </li>
                </ul>
            </div>
            <div class="sidebar-section">
                <div class="title">help</div>
                <ul class="list-unstyled">
                    <li>
                        <a href="https://reignministries.org/support-raising-help/" target="_blank">
                            <span class="icon-padding">
                                <i class="fas fa-dollar-sign fa-lg fa-fw"></i>
                            </span>Support Raising
                        <i class="fas fa-external-link-alt fa-sm fa-fw"></i></a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="icon-padding">
                                <i class="fas fa-question-circle fa-lg fa-fw"></i>
                            </span>Contact Us</a>
                    </li>
                </ul>
            </div>
            <div class="sidebar-section top-padding mobile-hide">
                <div class="title">downloads </div>
                <ul class="list-unstyled">
                    <li>
                        <a target="_blank" href="{{ Auth::user()->role->settings[0]['prep_packet'] }}">
                            <span class="icon-padding">
                                <i class="fas fa-arrow-alt-circle-down fa-lg fa-fw"></i>
                            </span>Prep-Packet
<i class="fas fa-external-link-alt fa-sm fa-fw"></i></a>
                    </li>
                    {{--
                    <li class="text-right">
                        <a href="#">See All</a>
                    </li> --}}
                </ul>
            </div>
            {{--
            <div class="sidebar-section mobile-hide">
                <div class="title top-padding">helpful links</div>
                <ul class="list-unstyled">
                    <li>here are some links </li>
                    <li>here are some links </li>
                    <li>here are some links </li>
                    <li class="text-right">
                        <a href="#">See All</a>
                    </li>
                </ul>
            </div> --}}
        </div>
        <div class="mobile-logout">
            @if (Auth::check())
            <a href="/myaccount">My Account</a>
            <a href="/logout">Logout</a> @else
            <a href="/login">Login</a> @endif
        </div>
    </div>
</div>
{{-- / Sidebar --}}