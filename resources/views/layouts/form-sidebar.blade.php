{{-- Sidebar --}}
<div class="sidebar" id="menu">
    <div class="sidebar-container form-sidebar">


            <section class="col-md-12 mb-2">
                    <transition enter-active-class="animated fadeIn">
                        <a class="btn btn-primary mt-5" href="/" role="button" v-if="ready"><i class="fas fa-chevron-circle-left pr-2"></i> Return</a>
                    </transition>
                </section>
       
        <div class="mobile-logout">
            @if (Auth::check())
            <a href="/myaccount">My Account</a>
            <a href="/logout">Logout</a>
            @else
            <a href="/login">Login</a>
            @endif
        </div>
    </div>
</div>
{{-- / Sidebar --}}