<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="../../favicon.ico">
    <title>My Missions Trip</title>

    <!-- Custom styles for this template -->
    <link href="{{ mix('/css/app.css') }}" rel="stylesheet"> {{-- Animate.css Library --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css"> {{-- fontawesome Webfont link --}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg"
        crossorigin="anonymous">

</head>

<body>

    @if(auth()->check())
    <script>
        const reg = {!! $currentReg !!} 
        const user = {!! Auth::user()->load('role') !!} 
        const donations = @php if( isset($donations)) echo $donations 

@endphp;
        const forms = @php if( isset($forms) )echo $forms 

@endphp;
    </script>
    @endif
    <div id="app">
        <reign-header></reign-header>
        <div id="panel">
            <section class="main col-lg-12">
                <router-view></router-view>
            </section>
        </div>

        @yield('sidebar')

        <portal-target name='modal' />
    </div>


    <script src="{{ mix('/js/app.js') }}"></script>
</body>

</html>