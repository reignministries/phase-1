@extends('layouts.master') 
@section('content')
    @include('layouts.header')
<div class="col-lg-12 main">
    <timeline-component :finished-support="finishedFundRaising" :step-status="{{ $stepStatus }}"></timeline-component>
    <div class="row">
        <div class="col-lg-6">
            {{-- Support Card --}}
            <transition enter-active-class="animated fadeIn">
                <quick-support-component :donations="donations" :cost="totalCost" v-if="ready"></quick-support-component>
            </transition>
        </div>
        <div class="col-lg-6 col-md-6">
            <transition enter-active-class="animated fadeIn">
                <form-status-component :forms='{{ $forms }}' v-if="ready"></form-status-component>
            </transition>
        </div>
    </div>
</div>
@endsection
 
@section('sidebar')
    @include('layouts.sidebar')
@endsection