@component('mail::message')
##Message from user:

{{$data->message}}

##Forms with errors:
@component('mail::panel')

@foreach ($forms as $form )
###{{$form['name']}}: 
-{{$form['error_message']}}

@endforeach


@endcomponent

@component('mail::button', ['url' => env('APP_URL').'/email/dashboard/'.$user->id])
View Dashboard
@endcomponent
