<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */


Route::get('/', 'DashboardController@index');
Route::get('/dashboard', 'DashboardController@index')->name('home');

/**
 * Sessions API for axios Requests
 */

 Route::prefix('api')->group(function(){
    /**
     * User related endpoints
     */
     Route::post('/user/change-password', 'UserController@changePassword');
     Route::post('/user/update', 'UserController@update');
     Route::post('/user/image/update','UserController@imageUpdate');


    // Registration endpoints
    Route::get('/registration/{id}/forms', 'RegistrationController@forms');
    Route::get('/registration/{id}/update-forms','RegistrationController@updateForms');

 });

// Donation Related
Route::get('support/donations', 'DashboardController@donations');

// Logout Current User
Route::get('logout', 'Auth\LoginController@logout');


// Registration Related
Route::get('current-registration', 'DashboardController@currentReg');
Route::get('current-registration/progress', 'DashboardController@progress');


// -------------------------Forms

// Detailed Registration
Route::get('forms/detailed-registration', 'Forms\DetailedRegController@create');
Route::get('forms/detailed-registration/answers', 'Forms\DetailedRegController@answers');
Route::post('forms/detailed-registration/store', 'Forms\DetailedRegController@store');



// Trip Related
Route::get('trips/current', 'TripController@currentTrips');
Route::get('trips/current/names', 'TripController@currentTripNames');


//Email Redirects
//TODO::Needs to be fleshsed out. 
Route::get('email/dashboard/{id}', 'EmailController@dashboardRedirect');

// default laravel auth creation
Auth::routes();


// Route::get('/{vue_capture?}', 'DashboardController@index')->where('vue_capture', '[\/\w\.-]*');