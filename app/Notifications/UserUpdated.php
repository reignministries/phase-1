<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserUpdated extends Notification
{
    use Queueable;

    protected $user, $changed;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, $changed)
    {
        $this->user = $user;
        $this->changed = $changed;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
       
        return (new MailMessage)
            ->subject('Participant Info Updated: '. $this->user->p_code)
            ->greeting($this->user->first . ' ' . $this->user->last . ': ' . $this->user->p_code)
            ->line('Some of the info for this participant has been updated on the online dashboard.')
            ->action('View Participant', url('/admin/resources/users/'.$this->user->id))
            ->salutation(' ');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
