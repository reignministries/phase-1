<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    public function forms(){
        return $this->belongsToMany(Form::class);
    }

    public function registrations(){
        return $this->hasMany(Registraion::class);
    }
}
