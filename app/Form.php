<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    public function divisions(){
        return $this->belongsToMany(Division::class);
    }

    public function byDivision(){
        $regDivisionId = 1;
        
        return $this->divisions()->where('division_id', $regDivisionId);
    }

    public function registration(){
        return $this->belongsToMany(Registration::class);
    }

}
