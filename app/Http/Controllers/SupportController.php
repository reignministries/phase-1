<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;

class SupportController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    
    public function show() {

        $reg =  Auth::user()->currentRegistration();            
        
        
        $stepStatus = json_encode($reg->timeline_status);


        return view('dashboard.support.index', compact('stepStatus'));
    }

      


}
