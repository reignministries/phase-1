<?php

namespace App\Http\Controllers\Forms;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;

class DetailedRegController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }



    public function create(){
        $user = Auth::user(); 
        $detailedReg = $user->currentRegistration()->detailedReg();
        $regId = $user->currentRegistration()->id;        
        
        if($detailedReg->exists() ){ 
           
            return view('forms.detailed-reg');            
            
        } else {

            // If there isn't an entry for the detailed registration already created.  Create one and mark that the detailed registration has been started
            $user->currentRegistration()->detailedReg()->firstOrCreate(['registration_id'=> $regId ]); 
            $currentReg = $user->currentRegistration();

            $currentReg->update([
                'timeline_status->detailed_reg->started' => true,
                'timeline_status->detailed_reg->name' => 'Detailed Registration'
            ]);            
            return view('forms.detailed-reg');
        }             

    }

    
    public function store(){
        $user = Auth::user(); 
        $regId = $user->currentRegistration()->id;  
        $detailedReg = DB::table('form_detailed_regs')->where('registration_id', $regId);
                
        $detailedReg->update([
            'alumni'=> request()->form['alumni'],           
            'trip_preference_1'=> json_encode(request()->form['trip_preference_1']),
            'trip_preference_2'=> json_encode(request()->form['trip_preference_2']),
            'current_page'=> request()->current_page,


            ]);
  
       


        return    request()->form['current_page'];
    
    }




    public function answers(){
        return Auth::user()->currentRegistration()->detailedReg()->first();
    }
}
