<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Notifications\UserUpdated;




class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function update()
    {
        
        //TODO: Abstract email to settings in database
        $admin = User::find(43);
        $user = Auth::user();
        $changed = [];
        /**
         * Validate Request then save request to logged in user
         */
        request()->validate([
            'first' => 'required',
            'middle' => 'required',
            'last' => 'required',
            'email' => 'required|email',
            'address_1' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip_code' => 'required|regex:/\b\d{5}\b/' 
        ]);


        foreach (request()->request as $key => $value) {
            if ($user->$key != request()->$key) {
                $changed[$key] = $value;
            }
        };


        if (request()->id == $user->id) {
            $user->first = request()->first;
            $user->middle = request()->middle;
            $user->last = request()->last;
            $user->email = request()->email;
            $user->address_1 = request()->address_1;
            $user->address_2 = request()->address_2;
            $user->city = request()->city;
            $user->state = request()->state;
            $user->zip_code = request()->zip_code;
            $user->save();
            $admin->notify(new UserUpdated($user, $changed));

            return $changed;
        } else {
            return 'no bueno mr hacker man';
        }


    }
    public function changePassword(Request $request)
    {
        /**
         * Checks to see that the requests current password matches
         * the logged in users current password and makes sure that
         * the new password doesn't match the current one 
         */

        $validator = Validator::make($request->all(), [
            'password' => [
                'required',
                'min:6',
                'confirmed',
                function ($attribute, $value, $fail) {
                    if (Hash::check($value, Auth::user()->password)) {
                        $fail("New password can't match old");
                    }
                },
            ],
            'current' => [
                'required',
                function ($attribute, $value, $fail) {
                    if (!Hash::check($value, Auth::user()->password)) {
                        $fail("Invalid current password");
                    }
                },
            ],
        ])->validate();

        $user = Auth::user();
        $user->password = bcrypt(request()->password);
        $user->save();

        return 'password succesfully updated!';


    }
    public function imageUpdate(Request $request)
    {

        $file = request()->file('avatar');
        $visability = 'public';
        $user = Auth::user();
        $url = Storage::putFileAs('images/avatars', $file, 'user-' . Auth::user()->id . '-avatar.'.$file->getClientOriginalExtension(), $visability);

        $user->avatar = Storage::url($url);
        $user->save();

        return 'picture updated';
    }
}
