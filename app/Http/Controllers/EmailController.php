<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\FormErrorResponse;
use App\User;
use Illuminate\Support\Facades\Auth;

class EmailController extends Controller
{
    public function errorResponse()
    {
        // TODO::secure with passport so people can't just ping this api and send emails.
        $defaultAdmin = 'admin@email.com';

        $participant = User::find(request()->userId);
        $request = request();
        $errorForms = request()->forms;

        \Mail::to($defaultAdmin)->send(new FormErrorResponse($participant, $request, $errorForms));
        return request();
    }

    public function dashboardRedirect($id)
    {
        if(!Auth::check()){
            return 'Needs to redirect to auth';
        } if(Auth::user()->role->id == 1) {
           return 'Needs to redirect to admin dashboard';
        } else {
            return redirect()->route('home');
        }



        
    }
}
