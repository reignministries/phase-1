<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\User;




class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $reg = Auth::user()->currentRegistration();

        $forms = $reg->forms()->select('name', 'id', 'completed', 'error', 'error_message', 'due_date')->get();

        $donations = json_encode(DB::table('donations')->where('p_code', Auth::user()->p_code)->orderBy('donation_date', 'desc')->get());


        $stepStatus = json_encode($reg->timeline_status);

        return view('dashboard.index', compact('stepStatus', 'forms', 'donations'));
    }

   

    // public function forms($divisionId)
        
    //     $data = Division::find($divisionId)->forms()->get();
                
    //     return $data;
    // }

    public function progress()
    {
        $data = Auth::user()->currentRegistration()->timeline_status;

        return $data;

    }
    public function donations()
    {
        $user = Auth::user()->p_code;

        $data = DB::table('donations')->where('p_code', $user)->orderBy('donation_date', 'desc')->get();

        return $data;

    }  



    // Test Function for end points
    public function ddjson()
    {
        return Auth::user()->load('role');
    }
}
