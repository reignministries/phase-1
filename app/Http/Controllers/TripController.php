<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Trip;

class TripController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function currentTrips() {
        $currentYear = 2018;
        $data = Trip::where('year', '=', $currentYear)->orderBy('name')->get();

        return $data;
    }
     public function currentTripNames() {       

       $currentYear = 2018;
        $data = Trip::where('year', '=', $currentYear)->select('id','name')->orderBy('name')->get();

        return $data;
    }
}
