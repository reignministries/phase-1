<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;
use Auth;
use Nova;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        view()->composer(['dashboard.index','dashboard.support.index', 'forms.detailed-reg'], function($view){
            $view->with('currentReg', Auth::user()->currentRegistration());
        });
        

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
         

    }
}
