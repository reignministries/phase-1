<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FormErrorResponse extends Mailable
{
    use Queueable, SerializesModels;

    public $user, $data, $forms;
    /**

     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($participant, $request, $errorForms)
    {
        $this->user = $participant;
        $this->data = $request;
        $this->forms = $errorForms;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->user->email)->markdown('emails.form-error-response');
    }
}
