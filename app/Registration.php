<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Forms\FormDetailedReg;
use Laravel\Scout\Searchable;

class Registration extends Model
{
    use Searchable;
    
    protected $guarded = [
        'user_id','role_id','begining_balance'
    ];

    protected $casts = [
        'timeline_status' => 'array',
        'timeline_settings' => 'array',
        'form_status' => 'array',
    ];
    

    public function user() {

        return $this->belongsTo(User::class);
        
    }

    public function trip(){
        return $this->belongsTo(Trip::class);
    }

    public function role(){
        return $this->belongsTo(Role::class);
    }

    public function division(){
        return $this->belongsTo(Division::class);
    }
    public function forms(){
        return $this->belongsToMany(Form::class)
                    ->withPivot('completed','error','error_message')
                    ->withTimestamps()
                    ->as('status');
    }
    
    
    // Attaches forms to the users registraion
    public function detailedReg() {
        return $this->hasOne(FormDetailedReg::class);
    }

}
