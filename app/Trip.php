<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Trip extends Model
{
    use Searchable;

    protected $casts = [];

    public function registration()
    {
        return $this->hasMany(Registration::class);
    }


}
