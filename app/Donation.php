<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
    protected $dates = [
        'donation_date',
    ];    
    
    public function user() {
        return $this->belongsTo(User::class, 'p_code');
    }
}
