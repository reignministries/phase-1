<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Laravel\Scout\Searchable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Auth\Passwords\CanResetPassword;

class User extends Authenticatable
{
    use Notifiable;
    use Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',
        'password',
        'settings',
        'address_1',
        'address_2',
        'city',
        'state',
        'zip_code',

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'name' => 'array',
        'address' => 'array'
    ];





    public function registrations()
    {
        return $this->hasMany(Registration::class);
    }

    public function currentRegistration()
    {
        // TODO::Make some sort of database conection
        return $this->registrations->first();
    }


    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function donations()
    {
        return $this->hasMany(Donation::class, 'p_code', 'p_code');
    }


}
