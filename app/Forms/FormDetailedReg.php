<?php

namespace App\Forms;
use App\Registration;

use Illuminate\Database\Eloquent\Model;

class FormDetailedReg extends Model
{

    protected $guarded = ['registration_id'];

    protected $casts = [
        'trip_preference_1'=>'array',
        'trip_preference_2'=>'array',
        
    ];

     public function registration(){
        return $this->belongsTo(Registration::class);
    }
}
